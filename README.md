# Weather Everyday Telegram Bot

The bot sends a daily weather forecast at 7 am each day. Also it sends current weather info, today and week forecast when user press specific buttons.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to install the software in req.txt.

```
pip install -r req.txt
```

### Installing

Add your own telegram-bot and Dark Sky (formal forecast.io) keys.

Run bot.py.

```
python bot.py
```

## Built With

* [python-telegram-bot](https://python-telegram-bot.org/) - The telegram bot wrapper used
* [Dark Sky API](https://darksky.net/dev) - Forecast provider
* [SQLite](https://sqlite.org/index.html) - The database used

## Author

* **Andrey Zavorotny** - *Initial work* - [andrey_zavorotny](https://gitlab.com/andrey_zavorotny)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* It is test assignment for an interview.

