#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Separate file for functions that do not depend on python-telegram-bot.

from sqlite3 import connect
from forecastio import load_forecast
import datetime

# Dark Sky good enought. Also has satisfying API and wrapper for it.
dark_sky_api_key = "TOKEN"

# Emoji implantation.
icon = {
    "clear-day": " ☀️",
    "clear-night": " 💫",
    "rain": "☔️",
    "snow": " 🌨",
    "sleet": " ⚪️",
    "wind": " 🌪",
    "fog": " 🌫",
    "cloudy": " ☁️",
    "partly-cloudy-day": " ⛅️",
    "partly-cloudy-night": " ☁️",
}

# General forecast list styling. Common for all forecast functions.
header = "{}     {} {}  Rain % mm  Wind\n"
template = "{:}{:} {:>2.0f}℃ {:>2.0f}℃ {:>2.0f}% {:>4.1f}mm {:>2.0f}m/s\n"


def execute_query(query: str):
    """Execute custom query."""
    try:
        connection = connect('users.db')
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()
        connection.close()
    except Exception as e:
        print("Error {}:".format(e.args[0]))
    return result


def get_current_weather(lt: int, ld: int):
    """Gether today weather with real & 'feels like' temperature,
    rain chance & intensity, wind speed info, weather icon,
    brief summary about the day weather."""
    forecast = load_forecast(dark_sky_api_key, lt, ld, units="si")

    wnow = forecast.currently()
    output = icon[wnow.icon] + " " + wnow.summary \
        + ".\n`The temperature is " + str(round(wnow.temperature)) \
        + "℃ and its feels like " + str(round(wnow.apparentTemperature)) \
        + "℃. The rain chance is " + str(wnow.precipProbability*100) \
        + "% with intensity " + str(round(wnow.precipIntensity, 1)) \
        + "mm. The wind speed is " + str(round(wnow.windSpeed, 1)) + "m/s.`"
    return output


def get_today_forecast(lt: int, ld: int):
    """Gether forecast for today from hour to hour starting from current hour
    with real & 'feels like' temperature, rain chance & intensity,
    wind speed info, weather icon, brief summary about the day weather."""
    forecast = load_forecast(dark_sky_api_key, lt, ld, units="si")
    # forecast.hourly() provides 48h data. So it should be limited.
    # Loop through the list and check timestamp if it is today.
    # And if it is future.
    today_data = [
        data for data in forecast.hourly().data
        if data.time.date() == datetime.datetime.today().date() and
        data.time > datetime.datetime.now()]
    # if data.time-datetime.datetime.today().replace(hour=0).days == 0]

    output = forecast.hourly().summary + "\n\n`" \
        + header.format("Time", "Temp", "Feel")
    for hourly_data in today_data:
        output += template.format(
            hourly_data.time.strftime("%H:%M"),
            icon[hourly_data.icon],
            hourly_data.temperature,
            hourly_data.apparentTemperature,
            hourly_data.precipProbability*100,
            hourly_data.precipIntensity,
            hourly_data.windSpeed,)
    return output+"`"


def get_week_forecast(lt: int, ld: int):
    """Gether forecast for the next 7 days with high & low temperature,
    rain chance & intence and wind speed information, weather icon,
    brief summary about week weather."""
    forecast = load_forecast(dark_sky_api_key, lt, ld, units="si")

    output = forecast.daily().summary + "\n\n`" \
        + header.format("Date", "High", "Low ")
    for hourly_data in forecast.daily().data[1:]:
        output += template.format(
            hourly_data.time.strftime("%a%d "),
            icon[hourly_data.icon].replace(" ", ""),
            hourly_data.temperatureHigh,
            hourly_data.temperatureLow,
            hourly_data.precipProbability*100,
            hourly_data.precipIntensityMax,
            hourly_data.windSpeed,)
    return output+"`"
