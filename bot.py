#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time
import datetime
import os
from telegram.ext import Updater
import telegram
from tools import *  # pylint: disable=unused-wildcard-import


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)
logger = logging.getLogger(__name__)


location_button = telegram.KeyboardButton("Send my location",
                                          request_location=True)
keyboard = telegram.ReplyKeyboardMarkup([[
    location_button, "Current Weather"], ["Today Forecast", "Week Forecast"]])


def start(bot, update):
    """Send initial message and request user location."""
    bot.send_message(update.message.chat.id,
                     text="From now the bot will provide you "
                     "with daily forecast every morning at 7am. "
                     "But before that you need to tell your location "
                     "by pressing the button or by typing it manually "
                     "like the following:\n /set latitude longitude",
                     reply_markup=telegram.ReplyKeyboardMarkup(
                         [[location_button]], resize_keyboard=True))


def set_location_by_telegram(bot, update):
    """Recieve latitude and longitude from built-in location service."""
    add_or_update_user(bot, update,
                       str(update.message.location["latitude"]),
                       str(update.message.location["longitude"]))


def set_location_manually(bot, update, args):
    """Resieve latitude and longitude from typed command args. Check input."""
    try:
        if -90 <= float(args[0]) <= 90 and -180 <= float(args[1]) <= 180:
            add_or_update_user(bot, update, args[0], args[1])
        else:
            # I see no need in creating own Exception.
            raise ValueError
    except (IndexError, ValueError):
        # Also rises when arguments number differ or if letters passed.
        bot.send_message(update.message.chat.id,
                         text="Try again. Example: /set 48.212 -15.777")


def add_or_update_user(bot, update, lt: str, lg: str):
    """Add user id, latitude and longitude to database, or update existed
    statement with new latitude and longitude. Show main keyboard."""
    execute_query("insert or replace into users_geo values({},{},{});"
                  .format(update.message.chat.id, lt, lg))
    bot.send_message(update.message.chat.id,
                     text="Got it! You can change your location anytime.",
                     reply_markup=keyboard, resize_keyboard=True)


def send_morning_forecast_to_all(bot, job):
    """Send today weather forecast to all users in database."""
    users = execute_query("select id,lt,ld from users_geo;")
    for user in users:
        bot.send_message(user[0],
                         text=get_today_forecast(user[1], user[2]),
                         parse_mode=telegram.ParseMode.MARKDOWN)


def send_current_weather(bot, update):
    """Send today weather."""
    send_message(bot, update, get_current_weather)


def send_today_forecast(bot, update):
    """Send forecast for today, hour by hour."""
    send_message(bot, update, get_today_forecast)


def send_week_forecast(bot, update):
    """Send forecast for the next 7 days, day by day."""
    send_message(bot, update, get_week_forecast)


def send_message(bot, update, forecast):
    """Send custom weather message."""
    lt, ld = execute_query("select lt,ld from users_geo where id = {};"
                           .format(update.message.chat.id))[0]
    bot.send_message(update.message.chat.id,
                     text=forecast(lt, ld),
                     parse_mode=telegram.ParseMode.MARKDOWN)


def error(update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Run bot. Set os timezone to 'Europe/Kiev'."""
    # Set Kiev timezone for the pythonanywhere server,
    # or morning messages will be send after noon.
    os.environ['TZ'] = 'Europe/Kiev'
    time.tzset()

    updater = Updater("966440843:AAEP7TXnzax_mVoCKlwzeqs0j8elbaqzN3o")
    dispatcher = updater.dispatcher

    dispatcher.add_handler(telegram.ext.CommandHandler(
        "start", start))
    dispatcher.add_handler(telegram.ext.CommandHandler(
        "set", set_location_manually, pass_args=True))
    dispatcher.add_handler(telegram.ext.MessageHandler(
        telegram.ext.Filters.location, set_location_by_telegram))
    dispatcher.add_handler(telegram.ext.MessageHandler(
        telegram.ext.Filters.regex('Current Weather'), send_current_weather))
    dispatcher.add_handler(telegram.ext.MessageHandler(
        telegram.ext.Filters.regex('Today Forecast'), send_today_forecast))
    dispatcher.add_handler(telegram.ext.MessageHandler(
        telegram.ext.Filters.regex('Week Forecast'), send_week_forecast))

    dispatcher.add_error_handler(error)

    updater.start_polling()

    # Run daily job.
    # Maybe shouldn`t send morning messages on holidays.
    # Or send but later, at 10am for example.
    updater.job_queue.run_daily(send_morning_forecast_to_all,
                                # days = (0, 1, 2, 3, 4, 5, 6),
                                time=datetime.time(7))

    updater.idle()


if __name__ == '__main__':
    main()
